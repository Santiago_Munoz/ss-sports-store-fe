export const dummyData = [
  { sku: 101, description: "El producto más chingon", price: 2000.0 },
  { sku: 555, description: "Golpe con de todo", price: 2000.0 },
  { sku: 522, description: "Galletas waffer", price: 500.0 },
  { sku: 202, description: "Gorra del bicho - premium edition", price: 560999.0 },
  { sku: 211, description: "Guallos ankara messi 2021", price: 10999.0 },
];
